"""The server handling commands and returning the response in JSON."""

import json
import os
import socket
import threading
import time
from cprint import *
from datetime import datetime

import config as cfg


class Server:
    def __init__(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ADDR = (cfg.SERVER, cfg.PORT)
        self.socket.bind(ADDR)
        self._start()

    def _start(self):
        """Start listening."""
        self.socket.listen()
        self.start_time = time.time()
        self.server_working = True
        self.username = None
        self.permission_group = None
        cprint.info(f"[Listening]: {cfg.SERVER}")
        while self.server_working:
            conn, addr = self.socket.accept()
            thread = threading.Thread(target=self._handle_client, args=(conn, addr))
            thread.start()
            self.connections_number = threading.activeCount() - 1
            print(f"[Active Connections]: {self.connections_number}")
        self.socket.close()

    def _handle_client(self, conn, addr):
        """Handle client."""
        print(f"[New connection]: {addr}")
        self.conn = conn
        self.connected = True
        while self.connected:
            msg = self._receive_message()
            print(f"[Received from {addr}]: {msg}")
            self._dispatcher(msg)
        conn.shutdown(socket.SHUT_RDWR)
        conn.close()
        print(f"[Disconnected]: {addr}")

    def _receive_message(self):
        """Receive message."""
        msg_length = self.conn.recv(cfg.HEADER).decode(cfg.FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = self.conn.recv(msg_length).decode(cfg.FORMAT)
            return msg.lower()

    def _send_message(self, msg):
        """Send msg to the client."""
        msg = json.dumps(msg)
        self.conn.send(msg.encode(cfg.FORMAT))

    def _dispatcher(self, msg):
        """Process incoming commands."""
        if msg == "!disconnect":
            self.connected = False
        elif msg == "!uptime":
            self._send_message(self.get_uptime())
        elif msg == "!stop":
            self.stop()
        elif msg == "!help":
            self._send_message(self.get_help())
        elif msg == "!info":
            self._send_message(self.get_info())
        elif msg == "!login":
            self._log_in()
        elif msg == "!logout":
            self._log_out()
        elif msg == "!priv":
            self._process_priv_msg()
        elif msg == "!mailbox":
            self._mailbox()
        elif msg == "!add_user":
            self.add_user()
        elif msg == "!delete_user":
            self.delete_user()
        else:
            self._send_message("No such command! Type !help for commands")

    def _get_users_data(self):
        """Load users.json file."""
        # TODO: add exception key
        try:
            with open("data/users.json", "r") as f:
                data = json.load(f)
        except:
            cprint.fatal("Could not open users.json")
            return False
        self.users = data
        return True

    def _get_user(self):
        """Prompt user to choose available user from users.json"""
        self._get_users_data()
        list_of_users = [user["username"] for user in self.users["users"]]
        list_of_users_print = [user for user in list_of_users if user != ""]
        self._send_message(f"Choose desired user: {list_of_users_print}")
        chosen_user = self._receive_message()
        if chosen_user not in list_of_users:
            self._send_message(f"No such user: {chosen_user}")
            return None
        else:
            # self._send_message(f"Chosen user: {chosen_user}")
            return chosen_user, list_of_users.index(chosen_user)

    def _process_priv_msg(self):
        """Get private message and save to data/messages.json."""
        if not self._log_in():
            print("You have to be logged in to send a message")
            return
        chosen_user, chosen_user_id = self._get_user()
        if len(self.users["users"][chosen_user_id]["messages"]) >= 5:
            print("Mailbox full")
            self._send_message(f"{chosen_user}'s mailbox is full, sorry!")
            return
        self._send_message(f"Send a message to {chosen_user}")
        user_message = self._receive_message()
        if len(user_message) > 255:
            cprint.err("Received message too long (>255)")
            self._send_message("Message too long (>255). Failed")
            return
        message = {
            "from": self.username,
            "date": datetime.now().ctime(),
            "content": user_message,
        }
        self.users["users"][chosen_user_id]["messages"].append(message)
        with open("data/users.json", "w+") as f:
            json.dump(self.users, f, indent=4)
        cprint.ok("Message saved")
        self._send_message(f"Message {user_message} has been sent")

    def _mailbox(self):
        """Open mailbox."""
        if not self._log_in():
            print("You have to be logged in to see mailbox")
            return
        mailbox = self.users["users"][self.userId]["messages"]
        self._send_message(mailbox)
        while True:
            command_mailbox = self._receive_message()
            if command_mailbox == "!exit":
                self._send_message("Mailbox closed")
                break
            elif command_mailbox[:7] == "!remove":
                # TODO: case when number not passed - regex?
                message_id = int(command_mailbox[-1]) - 1
                try:
                    removed_msg = self.users["users"][self.userId]["messages"].pop(
                        message_id
                    )
                except IndexError:
                    removed_msg = self.users["users"][self.userId]["messages"].pop()
                with open("data/users.json", "w+") as f:
                    json.dump(self.users, f, indent=4)
                self._send_message(f"Message {removed_msg} removed.")
            else:
                self._send_message(
                    "Type '!exit' to close mailbox. Type '!remove <1-5>' to remove desired message."
                )

    def _log_in(self):
        """Log in user."""
        if self.username:
            return True
        else:
            self._send_message("You have to login! Type user: ")
            username = self._receive_message()
            self._send_message("Type password: ")
            password = self._receive_message()
            if self._authorize(username, password):
                cprint.ok(f"[{username}] logged in")
                if len(self.users["users"][self.userId]["messages"]) > 4:
                    self._send_message(
                        "Logged in. Your mailbox is full! Delete some messages."
                    )
                else:
                    self._send_message(f"Logged in as [{username}]")
                return True
            else:
                cprint.warn("Failed")
                self._send_message("Failed")
                return False

    def _log_out(self):
        """Log out user"""
        if self.username is not None:
            self.username = None
            self.userId = None
            self.permission_group = None
            self._send_message("Succesfully logged out")
        else:
            self._send_message("Not logged in")

    def _authorize(self, username, password):
        """Check the credentials."""
        self._get_users_data()
        for record in self.users["users"]:
            if record["username"] == username:
                if record["password"] == password:
                    self.username = username
                    self.userId = record["userId"]
                    self.permission_group = record["permission_group"]
                    cprint.ok(f"[{username}] authorized")
                    return True
                else:
                    cprint.err("Wrong password")
        return False

    def add_user(self):
        """Add new user."""
        if self._log_in():
            if self.permission_group == 0:
                self._send_message("Choose name for a new user: ")
                new_username = self._receive_message()
                list_of_users = (user["username"] for user in self.users["users"])
                if new_username not in list_of_users:
                    self._send_message("Choose password: ")
                    new_password = self._receive_message()
                    self._send_message(
                        "Choose permissions 0 for admin, 1 for normal user"
                    )
                    new_permission_group = int(self._receive_message())
                    new_user = {
                        "userId": len(self.users["users"]),
                        "permission_group": new_permission_group,
                        "username": new_username,
                        "password": new_password,
                        "messages": [],
                    }
                    self.users["users"].append(new_user)
                    with open("data/users.json", "w+") as f:
                        json.dump(self.users, f, indent=4)
                    print(f"Added new user: {new_username}")
                    self._send_message(f"Added new user: {new_username}")
                    return
                else:
                    self._send_message(f"Username {new_username} already exists")
                    return
        self._send_message("Adding user failed")
        return

    def delete_user(self):
        """Delete user."""
        if self._log_in():
            if self.permission_group == 0:
                chosen_user, chosen_user_id = self._get_user()
                self._send_message(
                    f"Are you sure you want to delete: {chosen_user}? (y/n)"
                )
                if self._receive_message() == "y":
                    self.users["users"][chosen_user_id]["permission_group"] = ""
                    self.users["users"][chosen_user_id]["username"] = ""
                    self.users["users"][chosen_user_id]["password"] = ""
                    self.users["users"][chosen_user_id]["messages"] = []
                    with open("data/users.json", "w+") as f:
                        json.dump(self.users, f, indent=4)
                    print(f"User {chosen_user} deleted")
                    self._send_message(f"User {chosen_user} deleted")
                    return
        self._send_message("Deleting user failed")
        return

    def get_uptime(self):
        """Get time of server working in seconds."""
        uptime = int(time.time() - self.start_time)
        return {"uptime": f"Server is up for {uptime} seconds"}

    def get_info(self):
        """Get version of server, date of last modification."""
        modification_time = time.ctime(os.path.getmtime("server.py"))
        info = {
            "modification_time": modification_time,
            "active_connections": self.connections_number,
        }
        if self.permission_group == 0:
            info["users"] = self.users["users"]
        elif self.permission_group == 1:
            info["user"] = {
                "userId": self.userId,
                "username": self.username,
                "permission_group": self.permission_group,
            }
        else:
            info["user"] = None
        info.update(cfg.INFO)
        return info

    def get_help(self):
        """Get list of commands and brief note."""
        return cfg.COMMANDS

    def stop(self):
        """Stop server and all connected clients."""
        print("[Terminating server]")
        self.connected = False
        os._exit(0)


if __name__ == "__main__":
    server = Server()
